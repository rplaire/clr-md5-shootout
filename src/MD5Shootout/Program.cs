﻿using System;
using System.Diagnostics;
using System.Security.Cryptography;

namespace MD5Shootout
{
    class Program
    {
        private static readonly Random _rng = new Random();
        private static readonly Int32[] _sizes = new[] { 16, 64, 256, 1024, 4096, 16384, 39000, 65536, 262144, 613711, 2 << 20, 2 << 22, 2 << 24, 2 << 26, 2 << 27 };
        private static Byte[][] _data;
        private static Double[] _nativeMeasurements;
        private static Double[] _managedMeasurements;
        private static DateTime _wallTimeStart;
        private static DateTime _wallTimeEnd;

        static void Main(string[] args)
        {
            var clock = new Stopwatch();

            var native = new MD5CryptoServiceProvider();
            var managed = new MD5Managed();

            var sizeCount = _sizes.Length;
            _data = new Byte[sizeCount][];
            _nativeMeasurements = new Double[sizeCount];
            _managedMeasurements = new Double[sizeCount];

            Console.WriteLine("Creating random data input to hash.");

            var left = Console.CursorLeft;
            var top = Console.CursorTop;
         
            for (var i = 0; i < sizeCount; i++)
            {
                Console.SetCursorPosition(left, top);
                Console.WriteLine("Initializing {0} bytes of random data.", _sizes[i]);
                _data[i] = new Byte[_sizes[i]];
                _rng.NextBytes(_data[i]);
            }

            // warmup

            Console.WriteLine("Running single round for warmup purposes.");

            left = Console.CursorLeft;
            top = Console.CursorTop;
         
            for (var i = 0; i < sizeCount; i++)
            {
                Console.SetCursorPosition(left, top);
                Console.WriteLine("Computing hashes on {0} bytes.", _sizes[i]);
                native.ComputeHash(_data[i]);
                managed.ComputeHash(_data[i]);
            }

            // perform

            const Int32 RoundCount = 16;

            Console.WriteLine("Performing {0} rounds on {1} different sizes of data.", RoundCount, sizeCount);

            left = Console.CursorLeft;
            top = Console.CursorTop;

            _wallTimeStart = DateTime.UtcNow;

            for (var i = 0; i < RoundCount; i++)
            {
                Console.SetCursorPosition(left, top);
                Console.WriteLine("Computing Round: {0}", i + 1);

                var dataLeft = Console.CursorLeft;
                var dataTop = Console.CursorTop;

                for (var j = 0; j < sizeCount; j++)
                {
                    Console.SetCursorPosition(dataLeft, dataTop);
                    var data = _data[j];
                    Console.WriteLine("Computing hash on {0} bytes.", data.Length);

                    Byte[] managedValue;
                    Byte[] nativeValue;

                    if (_rng.NextDouble() < 0.5)
                    {
                        managedValue = runManaged(clock, managed, data, j);
                        nativeValue = runNative(clock, native, data, j);
                    }
                    else
                    {
                        nativeValue = runNative(clock, native, data, j);
                        managedValue = runManaged(clock, managed, data, j);
                    }

                    var managedValueCheck = new Guid(managedValue);
                    var nativeValueCheck = new Guid(nativeValue);

                    Console.WriteLine("Checking hashes are equal: \r\n{0:N}\r\n{1:N}", managedValueCheck, nativeValueCheck);

                    if (managedValueCheck != nativeValueCheck)
                    {
                        throw new InvalidOperationException("MD5 hash value disagreement.");
                    }
                }
            }

            _wallTimeEnd = DateTime.UtcNow;

            Console.WriteLine(" Started:\t{0}", _wallTimeStart);
            Console.WriteLine("Finished:\t{0}", _wallTimeEnd);
            Console.WriteLine("Native\t\tManaged");
           
            for (var i = 0; i < sizeCount; i++)
            {
                Debug.WriteLine("{0:N}\t\t{1:N}", _nativeMeasurements[i], _managedMeasurements[i]);
                Console.WriteLine("{0:N}\t\t{1:N}", _nativeMeasurements[i], _managedMeasurements[i]);
            }
        }

        private static Byte[] runNative(Stopwatch clock, MD5CryptoServiceProvider native, byte[] data, int j)
        {
            clock.Restart();
            var nativeValue = native.ComputeHash(data);
            clock.Stop();
            _nativeMeasurements[j] += clock.Elapsed.TotalMilliseconds * 0.0625;
            return nativeValue;
        }

        private static Byte[] runManaged(Stopwatch clock, MD5Managed managed, byte[] data, int j)
        {
            clock.Restart();
            var managedValue = managed.ComputeHash(data);
            clock.Stop();
            _managedMeasurements[j] += clock.Elapsed.TotalMilliseconds * 0.0625;
            return managedValue;
        }
    }
}
